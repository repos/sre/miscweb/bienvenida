# Annual report miscweb static websites

Blubber images for bienvenida static html sites.

Mirror of https://gerrit.wikimedia.org/r/wikimedia/campaigns/eswiki-2018

## Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target bienvenida -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/2014/`


## Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/bienvenida:<timestamp>`
